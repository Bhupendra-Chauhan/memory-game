const gameContainer = document.getElementById("game");

const easy_level = 6;
const medium_level = 9;
const hard_level = 12;

const gifs = [
  "./gifs/1.gif",
  "./gifs/2.gif",
  "./gifs/3.gif",
  "./gifs/4.gif",
  "./gifs/5.gif",
  "./gifs/6.gif",
  "./gifs/7.gif",
  "./gifs/8.gif",
  "./gifs/9.gif",
  "./gifs/10.gif",
  "./gifs/11.gif",
  "./gifs/12.gif"
];
const Moves = document.getElementById('moves');
const ScoreDisplay = document.getElementsByClassName('best-score');
const score = document.getElementById('score');
let level, leftCards, move = 0;
const PlayAgain = document.getElementById('reset');
let bestScore = JSON.parse(localStorage.getItem('bestScore'));


// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(gifArray) {
  while (gameContainer.firstChild) {
    gameContainer.removeChild(gameContainer.firstChild);
  }
  for (let gif of gifArray) {
    // create a new div
    const newDiv = document.createElement("div");
    const imgDiv = document.createElement('img');
    // give it a class attribute for the value we are looping over
    // newDiv.classList.add(color);
    imgDiv.setAttribute('src', gif);
    // call a function handleCardClick when a div is clicked on
    // newDiv.addEventListener("click", handleCardClick);
    newDiv.addEventListener("click", handleCardClick);
    newDiv.appendChild(imgDiv);
    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

const arrOfCards = []
function handleCardClick(event) {
  if (arrOfCards.includes(event)) {
    arrOfCards.pop();
  }
  else if (arrOfCards.length < 2) {
    arrOfCards.push(event)
    card = event.target;
    card.firstChild.style.visibility = 'visible';
    if (arrOfCards.length == 1) setScore();
    if (arrOfCards.length == 2) MatchCard();
  }
}

function Game_Start() {
  let cards = []
  while(cards.length < level){
    index = Math.floor(Math.random()*gifs.length);
    if(!cards.includes(gifs[index]))  cards.push(gifs[index])
  }
  cards = cards.concat(cards);
  let shuffledCards = shuffle(cards);
  createDivsForColors(shuffledCards);
}

const game = document.getElementById('menu');
game.addEventListener('submit', event => {
  setScore(0);
  event.preventDefault();
  const gameLevel = event.currentTarget.elements[0].value;
  if(gameLevel==='easy'){
    level=easy_level;
  }
  else if(gameLevel==='medium'){
    level=medium_level;
  }else{
    level=hard_level;
  }

  Game_Start();
  leftCards = level;
  
})
function setScore(num) {
  if (num !== 0) move += 1;
  else move = num;
  Moves.innerText = `Moves: ${move}`;
}





const Matched = [];
function MatchCard() {
  const card1 = arrOfCards[0].target.firstChild;
  const card2 = arrOfCards[1].target.firstChild;
  setTimeout(() => {
    if (card1 !== card2 && card1.src === card2.src) {
      Matched.push(card1, card2);
      card1.parentElement.style.pointerEvents = 'none';
      card2.parentElement.style.pointerEvents = 'none';
      leftCards -= 1;
    } else {
      card1.style.visibility = 'hidden';
      card2.style.visibility = 'hidden';
    }
    arrOfCards.pop();
    arrOfCards.pop();
    if (!leftCards) {
      PlayAgain.classList.toggle('hidden');
      if (!bestScore || move < bestScore.move || (bestScore.move === move)) {
        bestScore = JSON.stringify(move);
        localStorage.setItem('bestScore', '' + bestScore);
        bestScore = JSON.parse(bestScore);
      }
      score.innerText = `${move}`
      if(move<bestScore){
        bestScore=move;
      }
      ScoreDisplay[0].innerText = `${bestScore}`;
      document.getElementById('message-container').classList.remove('hidden')
    }
  }, 1000)
}

PlayAgain.addEventListener('click', function(){
  document.getElementById('message-container').classList.add('hidden');
})